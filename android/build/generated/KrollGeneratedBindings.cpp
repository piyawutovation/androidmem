/* C++ code produced by gperf version 3.0.1 */
/* Command-line: gperf -L C++ -E -t 'C:/Users/ADMINI~1/AppData/Local/Temp/Administrator/androidmem-generated/KrollGeneratedBindings.gperf'  */
/* Computed positions: -k'' */

#line 3 "C:/Users/ADMINI~1/AppData/Local/Temp/Administrator/androidmem-generated/KrollGeneratedBindings.gperf"


#include <string.h>
#include <v8.h>
#include <KrollBindings.h>

#include "com.sam.androidmem.AndroidmemModule.h"
#include "com.sam.androidmem.ExampleProxy.h"


#line 14 "C:/Users/ADMINI~1/AppData/Local/Temp/Administrator/androidmem-generated/KrollGeneratedBindings.gperf"
struct titanium::bindings::BindEntry;
/* maximum key range = 5, duplicates = 0 */

class AndroidmemBindings
{
private:
  static inline unsigned int hash (const char *str, unsigned int len);
public:
  static struct titanium::bindings::BindEntry *lookupGeneratedInit (const char *str, unsigned int len);
};

inline /*ARGSUSED*/
unsigned int
AndroidmemBindings::hash (register const char *str, register unsigned int len)
{
  return len;
}

struct titanium::bindings::BindEntry *
AndroidmemBindings::lookupGeneratedInit (register const char *str, register unsigned int len)
{
  enum
    {
      TOTAL_KEYWORDS = 2,
      MIN_WORD_LENGTH = 31,
      MAX_WORD_LENGTH = 35,
      MIN_HASH_VALUE = 31,
      MAX_HASH_VALUE = 35
    };

  static struct titanium::bindings::BindEntry wordlist[] =
    {
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 17 "C:/Users/ADMINI~1/AppData/Local/Temp/Administrator/androidmem-generated/KrollGeneratedBindings.gperf"
      {"com.sam.androidmem.ExampleProxy", ::com::sam::androidmem::androidmem::ExampleProxy::bindProxy, ::com::sam::androidmem::androidmem::ExampleProxy::dispose},
      {""}, {""}, {""},
#line 16 "C:/Users/ADMINI~1/AppData/Local/Temp/Administrator/androidmem-generated/KrollGeneratedBindings.gperf"
      {"com.sam.androidmem.AndroidmemModule", ::com::sam::androidmem::AndroidmemModule::bindProxy, ::com::sam::androidmem::AndroidmemModule::dispose}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].name;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
#line 18 "C:/Users/ADMINI~1/AppData/Local/Temp/Administrator/androidmem-generated/KrollGeneratedBindings.gperf"

